# Pdf Booklet Maker

Python script to transform a pdf file into a pdf booklet ready to print.

## Usage 

``` 
    ./pybooklet.py -i <input> -o <output>
```
