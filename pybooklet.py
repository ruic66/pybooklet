#!/usr/local/bin/python3

from PyPDF2 import PdfFileWriter, PdfFileReader

import sys 
import getopt

''' Gather command line information'''
opts, args = getopt.getopt(sys.argv[1:], 'i:o:')
opts = dict(opts)

output = opts.get('-o', 'booklet.pdf')
input = opts.get('-i', None)

file = open(input, 'rb')

booklet = PdfFileWriter()
pdf = PdfFileReader(file)

numPages = pdf.getNumPages()
bookletPages = int(pdf.getNumPages() / 2)

print('=== PDF BOOKLET MAKER ===', end='\n\n')
print(f'This pdf contains {numPages} pages.')
print(f'The resulting booklet will contain {bookletPages} pages.', end='\n\n')

for pageN in range(0, bookletPages):
    print(f'Making page number {pageN + 1}')
    page1 = pdf.getPage(pageN)
    booklet.addBlankPage(page1.mediaBox.getWidth() * 2, page1.mediaBox.getHeight())
    page2 = pdf.getPage(numPages - pageN - 1)
    if (pageN % 2 == 0):
        booklet.getPage(pageN).mergeTranslatedPage(page1, page1.mediaBox.getWidth(), 0)
        booklet.getPage(pageN).mergeTranslatedPage(page2, 0, 0)
    else: 
        booklet.getPage(pageN).mergeTranslatedPage(page1, 0, 0)
        booklet.getPage(pageN).mergeTranslatedPage(page2, page1.mediaBox.getWidth(), 0)

outputStream = open(output, "wb")
booklet.write(outputStream)
outputStream.close()

print('\n=== DONE ===')

